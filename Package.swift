// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "TouchcodeSDK",
    platforms: [
        .iOS(.v10)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "TouchcodeSDK",
            targets: ["TouchcodeSDK"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
      .package(
          name: "Alamofire",
          url: "https://github.com/Alamofire/Alamofire.git", .exact("4.9.1")
      ),
      .package(
          name: "CryptoSwift",
          url: "https://github.com/krzyzanowskim/CryptoSwift.git", .exact("1.3.3")
      ),
      .package(
          name: "NetUtils",
          url: "https://github.com/svdo/swift-netutils.git", .exact("4.1.0")
      ),
      .package(
          name: "TouchcodeDecoder",
          url: "https://gitlab.com/touchcode/touchcodedecoder", .exact("1.0.12")
      )
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "TouchcodeSDK",
            dependencies: ["TouchcodeDecoder", "CryptoSwift", "NetUtils", "Alamofire"]),
    ]
)
